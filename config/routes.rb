Rails.application.routes.draw do
  
  get 'static/privacy'

# Fb Authentication
  get 'auth/index'
  get 'auth/new'

  root to: 'visitors#index'

# Search bar  
  get 'visitors/search', to: 'visitors#search', :as => 'search'
  post 'visitors/track', to: 'visitors#track', :as => 'tracks'

  match 'auth/:provider/callback', to: 'sessions#create', via: [:get, :post]
  match 'auth/failure', to: redirect('/'), via: [:get, :post]
  match 'signout', to: 'sessions#destroy', as: 'signout', via: [:get, :post]

  get 'privacy-policy', to: 'static#privacy', :as => 'privacy'
  
end
