class User < ActiveRecord::Base
  def self.from_omniauth(auth)
  	where(provide: auth.provider, uid: auth.uid).first_or_create do |user|
	  user.provide = auth.provider
	  user.uid = auth.uid
	  user.name = auth.info.name
	  user.oauth_token = auth.credentials.token
	  user.oauth_expires_at = Time.at(auth.credentials.expires_at)
	  user.save!
	end
  end
end
